//To create a provider and consume by components

import React from 'react';

//Creates a contaxt object
	// Context object as the name state is a ata type of an object that can be used to store information that can be shared to other components within app.
	// The context object is a different approach to pasing information between components and allows us for easier access by avoiding the use of prop drilling.

const UserContext = React.createContext();

//The "Provider" component allows other components to consume the context object and supply the necessary information needed to the context object.
export const UserProvider = UserContext.Provider;

export default UserContext;