import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Register(){

	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);

	const {user} = useContext(UserContext);
	// console.log(user);


	// console.log(email);
	// console.log(password1);
	// console.log(password2);

	// simulation of user registration

	function registerUser(e){
		e.preventDefault();

		// clear input fields
		setEmail('');
		setPassword1('');
		setPassword2('');

		alert('Thank you for registering!');
	};

	useEffect(() => {

		if ((email !== '' && password1 !== '' && password2 !== '')
			&& (password1 === password2)) {
			setIsActive(true);
		} else {
			setIsActive(false);
		};

	}, [email, password1, password2])
	// (useEffect looks at changes to anything listed in the array.)

	return(

		(user.id !== null) ?

		<Navigate to="/courses"/>

		:

		<Form onSubmit={e => registerUser(e)}>
			<h1 className="mt-3">Register</h1>
			<Form.Group controlId="userEmail" className="mb-2">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type = "email"
					placeholder = "Enter you email here."
					value = {email}
					onChange = {e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					We will never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password1" className="mb-3">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type = "password"
					placeholder = "Input your password here."
					value = {password1}
					onChange = {e => setPassword1(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password2">
				<Form.Label>Confirm Password</Form.Label>
				<Form.Control
					type = "password"
					placeholder = "Input your password again."
					value = {password2}
					onChange = {e => setPassword2(e.target.value)}
					required
				/>
			</Form.Group>

			{/*conditionally rendering submit button based on isActive state*/}
			{/*(ternary operators : ? (if) : (else))*/}

			{ isActive ?
				<Button variant="primary" type="submit" id="submitBtn" className="my-3">
					Submit
				</Button>
				:
				<Button variant="danger" type="submit" id="submitBtn" className="my-3" disabled>
					Submit
				</Button>
			}
		</Form>
	);
};
