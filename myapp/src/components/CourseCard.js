import {useState, useEffect} from "react";
import {Card, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'


export default function CourseCard ({courseProp}){
	//console.log(courseProp)

// 	const [count, setCount] = useState(0)
// 	//syntax const [getter, setter] = useState(initlaValueOfGetter)

// 	function enroll(){
// 		setCount(count + 1)
// 		//console.log('Enrollees' + count)
// 	};

// // A C T I V I T Y 2

// 	const [seats, setSeats] = useState(30)
// 	//const[isOpen, setIsOpen] = useState(true)

// 	function seatAvailable(){
// 		setSeats (seats - 1)
// 		console.log('Seat' + count)
// 	};

	// useEffect(() => {
	// 	if (seats === 0){
	// 		alert("No more seats available")
	// 	}
	// }, [seats])
	

	const {name, description, price, _id} = courseProp

	return(
	<Card>
	  <Card.Header as="h5">Courses</Card.Header>
	  <Card.Body>
	    <Card.Title>{name}</Card.Title>
	    	<Card.Subtitle>Description:</Card.Subtitle>
	    	<Card.Text>{description}</Card.Text>
	    	<Card.Subtitle>Price:</Card.Subtitle>
	       	<Card.Text>{price}</Card.Text>
	       	<Button variant="primary" as={Link} to={`/courses/${_id}`}>See Details</Button>
	  </Card.Body>
	</Card>
	)
}

	       	// <Card.Text>Enrollees: {count}</Card.Text>
	       	// <Card.Text>Seat/s Available: {seats}</Card.Text>
	      	//<Button variant="dark" onClick={(event) => [enroll(), seatAvailable()]} disabled={seats <= 0}>Enroll Now!</Button>