import {Row, Col} from 'react-bootstrap'
import {Link} from 'react-router-dom'



export default function ErrorBanner (){
	return (
		<Row>
			<Col className="p-5">
				<h1>PAGE NOT FOUND</h1>
				<Link to="/">Click to homepage</Link>
			</Col>
		</Row>
		)
}
