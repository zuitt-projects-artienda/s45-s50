import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css'

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// const name = "Kimberluh";

// const user = {
//   firstName: "Leni",
//   lastName: "Robredo"
// }

// const formatName = (user) => {
//   return `${user.firstName} ${user.lastName}`
// } 

// //JSX
// const element = <h1> Hello my name is, {formatName(user)}!</h1>
// ReactDOM.render(
//   element,
//   document.getElementById("root")
//   )